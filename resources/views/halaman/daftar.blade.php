
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First name:</label>
        <br>
        <input type="text" name="first_name">
        <br><br>
        <label>Last name:</label>
        <br>
        <input type="text" name="last_name">
        <br><br>
        <label>Gender:</label>
        <br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br>
        <input type="radio" name="gender" value="Other">Other <br>
        <br>
        <label>Nationality:</label>
        <br>
        <select name="nationality" id="">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporeans">Singaporeans</option>
            <option value="Malaysians">Malaysians</option>
            <option value="Australian">Australian</option>
        </select>
        <br>
        <br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="bahasa" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="English">English <br>
        <input type="checkbox" name="bahasa" value="Other">Other <br>
        <br>
        <label>Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>